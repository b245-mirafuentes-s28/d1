// CRUD Operations
	// CRUD operation is the heart of any backend application
	// mastering the CRUD operation is essential for any developer
	// This helps in building character and increasing exposure to logical statements that will help us manipulate
	// mastering the CRUD operations of any languages makes us a valuable developer and makes the work easier for us to deal with huge amounts of informations

	// [Section] Create(Inserting documents);
		// since MongoDB deals with object as it's structure for documents, we can easily create the by providing objects
		// mongoDB shell also uses javascript for it's syntax which makes it convinient for us to understand its code

		// Insert one document
			/*
				Syntax:
					db.collectionName.insertOne({object/document})
			*/
		db.users.insertOne({
			firstName: "Jane",
			lastName: "Doe",
			age: 21,
			contact: {
				phone: "1234567890",
				email: "janedoe@gmail.com"
			},
			courses: ["CSS", "JavaScript", "Python"],
			department: "none"
		});

		// Insert Many
			/*
				db.users.insertMany([{objectA}, {objectB}])
			*/
		db.users.insertMany([{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact: {
				phone: "87654321",
				email: "stephenhawking@gmail.com"
			},
			courses: ["Pyhton", "React", "PHP"],
			department: "none"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "87654321",
				email: "neilarmstrong@gmail.com"
			},
			courses: ["Pyhton", "React", "laravel"],
			department: "none"
		}
		])

		db.users.insertOne({
			firstName: "Jane",
			lastName: "Doe",
			age: 21,
			contact: {
				phone: "1234567890",
				email: "janedoe@gmail.com"
			},
			courses: ["CSS", "JavaScript", "Python"]
		});

		db.users.insertOne({
			lastName: "Doe",
			age: 21,
			contact: {
				phone: "1234567890",
				email: "janedoe@gmail.com"
			},
			courses: ["CSS", "JavaScript", "Python"],
			firstName: "Jane",
		});

		db.users.insertOne({
			firstName: "Jane",
			lastName: "Doe",
			age: 21,
			contact: {
				phone: "1234567890",
				email: "janedoe@gmail.com"
			},
			courses: ["CSS", "JavaScript", "Python"],
			gender: "female"
		});

		db.users.insertOne({
			firstName: "Jane",
			lastName: "Doe",
			age: 21,
			contact: {
				phone: "1234567890",
				phone: "123456789011",
				email: "janedoe@gmail.com"
			},
			courses: ["CSS", "JavaScript", "Python"],
			gender: "female"
		});

		// [Section] Find (read)

			/*
				Syntax:
					// it will show us all the documents in our collection
					db.collectionName.find();
					// it will show us all the documets that has the given fieldset
					db.collectionName.find({field: value})
			*/

		db.users.find();

		db.users.find({age: 76});

		db.users.find({firstName: "jane"});

		db.users.find({firstName: "Jane"});


			// finding documents using multiple fieldset

		/*
			Syntax:
				db.collectionName.find({fieldA: valueA, fieldB: valueB})
		*/

		db.users.find({lastName: "Armstrong", age: 82});

		// [Section] Updating Documents

		// add document
		db.users.insertOne({
			firstName: "Test",
			lastName: "Test",
			age: 0,
			contact: {
				phone: "00000000",
				email: "test@gmail.com"
			},
			courses: [],
			department: "none"
		});

			// updateOne
				/*
					syntax:
					db.collectionName.updateOne({criteria}, {$set: {field:value}})
				*/

		db.users.updateOne({
			firstName: "Jane"
		},
		{$set:{
			lastName: "Hawking"
		}
		})

			// updating multiple documents
				/*
					syntax:
						db.collectionName.updateMany({criteria}, {
						$set: {
							field: value
						}
						})
				*/

		db.users.updateMany({
			firstName: "Jane"
		},
		{
			$set: {
				lastName: "Wick",
				age: 25
			}
		})

		db.users.updateMany({department: "none"},{
			$set: {
				department: "Hr"
			}
		})

		// replacing the old document
			/*
				db.users.replaceOne({criteria}, {document/objectToReplace})
			*/

		db.users.replaceOne({firstName: "Test"}, {
			firstName: "William",
			lastName: "Mirafuentes"
		})

		// [Section] deleting documents

			// deleting single document
			/*
				Syntax:
					db.collectionName.deleteOne({criteria});
			*/
		db.users.deleteOne({firstName: "Jane"})

		// delete many
			/*
				syntax:
					db.collectionName.deleteMany({criteria})
			*/

		db.users.deleteMany({firstName: "Jane"})

		// reminder
			// db.users.deleteMany()
			// remind: dont forger to add criteria

		// [Section] Advanced Queries
			//embedded - object
			//nested field - array
		// query on an embedded document
		db.users.find({"contact.phone": "87654321"})

		// query an array with exact elements
		db.users.find({courses: ["Pyhton", "React", "PHP"]})